#include <windows.h>
//#include <tchar.h>
#include "..\include\adl_sdk.h"
//#include <map>

#include <stdio.h>

typedef int (*ADL_DISPLAY_WRITEANDREADI2C)(int iAdapterIndex, ADLI2C *plI2C);
typedef int (*ADL_MAIN_CONTROL_CREATE)(ADL_MAIN_MALLOC_CALLBACK callback, int iEnumConnectedAdapters);
typedef int (*ADL_MAIN_CONTROL_DESTROY)();

ADL_DISPLAY_WRITEANDREADI2C ADL_Display_WriteAndReadI2C;
ADL_MAIN_CONTROL_CREATE ADL_Main_Control_Create;
ADL_MAIN_CONTROL_DESTROY ADL_Main_Control_Destroy;

ADL_CONTEXT_HANDLE context = NULL;

// Memory allocation function
void *__stdcall ADL_Main_Memory_Alloc(int iSize) {
  void *lpBuffer = malloc(iSize);
  return lpBuffer;
}

// Optional Memory de-allocation function
void __stdcall ADL_Main_Memory_Free(void *lpBuffer) {
  if (NULL != lpBuffer) {
    free(lpBuffer);
    lpBuffer = NULL;
  }
}

int InitADL() {
  HINSTANCE hDLL;

  hDLL = LoadLibrary(L"atiadlxx.dll");

  if (NULL == hDLL) {
    printf("ADL library not found!\n");
    return ADL_ERR;
  }

  ADL_Display_WriteAndReadI2C = (ADL_DISPLAY_WRITEANDREADI2C)GetProcAddress(hDLL, "ADL_Display_WriteAndReadI2C");
  ADL_Main_Control_Create = (ADL_MAIN_CONTROL_CREATE)GetProcAddress(hDLL, "ADL_Main_Control_Create");
  ADL_Main_Control_Destroy = (ADL_MAIN_CONTROL_DESTROY)GetProcAddress(hDLL, "ADL_Main_Control_Destroy");

  if (NULL == ADL_Display_WriteAndReadI2C ||
      NULL == ADL_Main_Control_Create ||
      NULL == ADL_Main_Control_Destroy)
  {
    printf("ADL's API is missing!\n");
    return ADL_ERR;
  }

  return ADL_OK;
}

#define I2C_DATA_LENGTH 3

ADLI2C* CreateADLI2C() {
  char *pcData = (char *)calloc(I2C_DATA_LENGTH, sizeof(char));
  ADLI2C *plI2C = (ADLI2C *)calloc(1, sizeof(ADLI2C));
  plI2C->pcData = pcData;
  plI2C->iDataSize = I2C_DATA_LENGTH;
  return plI2C;
}

void FreeADLI2C(ADLI2C* plI2C) {
  free(plI2C->pcData);
  free(plI2C);
}

int WriteOffset(int offset, unsigned char dataA, unsigned char dataB, unsigned char dataC) {
  ADLI2C *plI2C = CreateADLI2C();
  plI2C->iSize = 32;
  plI2C->iLine = 1;
  plI2C->iAddress = 69;
  plI2C->iOffset = offset;
  plI2C->iAction = ADL_DL_I2C_ACTIONWRITE;
  plI2C->iSpeed = 150;
  plI2C->iDataSize = 3;
  plI2C->pcData[0] = dataA;
  plI2C->pcData[1] = dataB;
  plI2C->pcData[2] = dataC;

  int result = -1;

  result = ADL_Display_WriteAndReadI2C(0, plI2C);
  if (result != ADL_OK) {
    printf("Failed to write to offset=%d, error code=%d.\n", offset, result);
  } else {
    Sleep(50);
  }
  FreeADLI2C(plI2C);

  return result;
}

int ReadOffset(int offset, char *data) {
  ADLI2C *plI2C = CreateADLI2C();
  plI2C->iSize = 32;
  plI2C->iLine = 1;
  plI2C->iAddress = 68;
  plI2C->iOffset = offset;
  plI2C->iAction = ADL_DL_I2C_ACTIONREAD_REPEATEDSTART;
  plI2C->iSpeed = 150;
  plI2C->iDataSize = 3;
  plI2C->pcData[0] = 0;
  plI2C->pcData[1] = 0;
  plI2C->pcData[2] = 0;

  int result = -1;

  result = ADL_Display_WriteAndReadI2C(0, plI2C);
  if (result != ADL_OK) {
    printf("Failed to read from offset=%d, error code=%d.\n", offset, result);
  } else {
    data[0] = plI2C->pcData[0];
    data[1] = plI2C->pcData[1];
    data[2] = plI2C->pcData[2];
    Sleep(50);
  }
  FreeADLI2C(plI2C);

  return result;
}

int WriteSettingsSingleColor() {
  return WriteOffset(
    1, // offset. 1 = settings
    1, // mode. 1=single color
    100, // brightness. 100=full
    5 // speed. 5=default, irrelevant for this mode
  );
}

int WriteLed(int ledIdx, unsigned char r, unsigned char g, unsigned char b) {
  int offset = ledIdx + 2;
  if (offset < 2 || offset > 12) {
    printf("Tried to write LED stuff to offset=%d. Don't do that.\n", offset);
    return ADL_ERR;
  }

  return WriteOffset(offset, r, g, b);
}

int WriteFourteenLed(unsigned char r, unsigned char g, unsigned char b) {
  return WriteOffset(14, r, g, b);
}

int ExamineSyncMobo() {
  int result;
  char data[3];
  result = ReadOffset(146, data);
  if (result != ADL_OK) {
    return result;
  }

  printf("Read mobo sync data=[%d,%d,%d]\n", data[0], data[1], data[2]);

  return ADL_OK;
}

int TestLedSequence() {
  int result;
  printf("Writing settings...\n");
  result = WriteSettingsSingleColor();
  if (result != ADL_OK) {
    return result;
  }

  printf("Flattening colors...\n");
  for (int ledIdx = 0; ledIdx < 11; ledIdx++) {
    result = WriteLed(ledIdx, 0, 0, 0);
    if (result != ADL_OK) {
      return result;
    }
  }

  Sleep(3000);

  printf("Testing LEDs one by one.\n");

  for (int ledIdx = 0; ledIdx < 11; ledIdx++) {
    printf("Showing only LED %d.\n", ledIdx);
    // set last off
    if (ledIdx != 0) {
      result = WriteLed(ledIdx - 1, 0, 0, 0);
      if (result != ADL_OK) {
        return result;
      }
    }
    // set this on
    result = WriteLed(ledIdx, 0xff, 0, 0xff);
    if (result != ADL_OK) {
      return result;
    }
    Sleep(1000);
  }

  return result;
}

int TestFourteen() {
  int result;
  printf("Writing settings...\n");
  result = WriteSettingsSingleColor();
  if (result != ADL_OK) {
    return result;
  }

  printf("Magentaing fourteen...\n");
  result = WriteFourteenLed(255, 0, 255);
  if (result != ADL_OK) {
    return result;
  }

  return result;
}

int TestTransRights() {
  int result;
  printf("Writing settings...\n");
  result = WriteSettingsSingleColor();
  if (result != ADL_OK) {
    return result;
  }
  printf("Clearing all LEDs...\n");
  WriteFourteenLed(0, 0, 0); if (result != ADL_OK) { return result; }

  printf("Waiting a bit...\n");
  Sleep(5000);

  printf("Showing pride...\n");
  //#define TRANS_A 88, 200, 242
  //#define TRANS_B 237, 164, 178
  #define TRANS_A 0, 233, 255
  #define TRANS_B 255, 38, 81
  #define TRANS_C 255, 255, 255
  WriteLed( 0, TRANS_A); if (result != ADL_OK) { return result; }
  WriteLed( 1, TRANS_B); if (result != ADL_OK) { return result; }
  WriteLed( 2, TRANS_B); if (result != ADL_OK) { return result; }
  WriteLed( 3, TRANS_B); if (result != ADL_OK) { return result; }
  WriteLed( 4, TRANS_C); if (result != ADL_OK) { return result; }
  WriteLed( 5, TRANS_C); if (result != ADL_OK) { return result; }
  WriteLed( 6, TRANS_C); if (result != ADL_OK) { return result; }
  WriteLed( 7, TRANS_B); if (result != ADL_OK) { return result; }
  WriteLed( 8, TRANS_B); if (result != ADL_OK) { return result; }
  WriteLed( 9, TRANS_B); if (result != ADL_OK) { return result; }
  WriteLed(10, TRANS_A); if (result != ADL_OK) { return result; }

  return result;
}

// int argc, char* argv[], char* envp[]
int main(int, char**, char**) {
  printf("Sizeof int = %zu\n", sizeof(int));
  printf("Sizeof char* = %zu\n", sizeof(char*));
  printf("Sizeof ADLI2C = %zu\n", sizeof(ADLI2C));

  if (ADL_OK != InitADL()) {
    printf("Couldn't init ADL DLL!\n");
    return -1;
  }

  printf("Legacy ADL init...\n");
  if (ADL_OK != ADL_Main_Control_Create(ADL_Main_Memory_Alloc, 1)) {
    printf("Can't init ADL!\n");
    return -1;
  }

  printf("Running the ADL code...\n");
  if (ADL_OK != TestTransRights()) {
    printf("The LEDs are stuck now, please suspend the PC if you want to try again!\n");
    return -1;
  }

  if (ADL_OK != ADL_Main_Control_Destroy()) {
    printf("Can't destroy legacy ADL context!\n");
    return -1;
  }
  printf("Legacy ADL context shut down.\n");

  return 0;
}