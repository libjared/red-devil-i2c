# How to reverse engineer ADL/I2C apps

This is what I did to reverse engineer DevilZone, which uses AMD Display Library (ADL) to exchange data with the GPU (via I2C) to set LEDs.

1. Get [API Monitor](http://www.rohitab.com/downloads), alpha r13 or later
2. Download this ADL API definitions file that I made by looking at the ADL SDK docs: [adl.xml](adl.xml)
3. Save it to `C:\Program Files\rohitab.com\API Monitor\API\AMD\` (must create `AMD` directory)
4. Launch API Monitor in 32-bit mode (because DevilZone.exe is a 32-bit app). Might need to run as admin.
5. In the *API Filter* pane, check the *AMD ADL* checkbox.
6. In the *Monitored Processes* pane, click *Monitor New Process*.
7. Find DevilZone.exe and click OK.
8. In the *Summary* pane, observe the function calls.
9. Click on a row and observe the parameters in the *Parameters* pane.

After that, regular reverse-engineering skills apply: experiment with the app and try to hypothesize what API calls it will make.